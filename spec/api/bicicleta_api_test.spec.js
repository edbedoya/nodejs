var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");
var mongoose = require("mongoose");

var base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", () => {
  beforeAll(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/red_bicicletas";

      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      mongoose.set("useCreateIndex", true);

      var db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB connection error: "));
      db.once("open", function () {
        console.log("We are connected to test database!");
        done();
      });
    });
  });
  afterAll((done) => {
    Bicicleta.deleteMany({}, function (error, success) {
      if (error) console.log(error);
      done();
    });
  });

  describe("GET BICICLETAS /", () => {
    it("STATUS 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicleta.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETASS /create", () => {
    it("STATUS 200", (done) => {
      var headers = {
        "content-type": "application/json"
      };
      var aBici =
        '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';

      request.post({
          headers: headers,
          url: base_url + "/create",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          var result = JSON.parse(body);
          expect(result.bicicleta.color).toBe("rojo");
          expect(result.bicicleta.ubicacion[0]).toBe(-34);
          expect(result.bicicleta.ubicacion[1]).toBe(-54);
          done();
        }
      );
    });
  });

  describe("UPDATE BICICLETASS /update", () => {
    it("STATUS 204", (done) => {
      var headers = {
        "content-type": "application/json"
      };
      var aBiciId = '{"id": 10, "color": "violeta"}';

      request.put({
          headers: headers,
          url: base_url + "/update",
          body: aBiciId,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(204);          
          done();
        }
      );
    });
  });

  describe("DELETE BICICLETASS /delete", () => {
    it("STATUS 204", (done) => {
      var headers = {
        "content-type": "application/json"
      };
      var aBiciId = '{"id": 10}';

      request.delete({
          headers: headers,
          url: base_url + "/delete",
          body: aBiciId,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(204);
          done();
        }
      );
    });
  });
});