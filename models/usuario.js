var mongoose = require("mongoose");
var Reserva = require("./reserva");
const Token = require("./token")
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const mailer = require('../mailer/mailer');

const saltRounds = 10;

var Schema = mongoose.Schema;


const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioShema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: [true, 'El email es obligatorio'],
        validate: [validateEmail, 'Por favor ingrese un email válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio'],
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioShema.plugin(uniqueValidator, {
    message: 'El {PATH} ya existe con otro usuario'
});

usuarioShema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioShema.methods.validatePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioShema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta,
    });
    reserva.save(cb);
};

usuarioShema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex')
    });
    const emailDestination = this.email;

    token.save(function (err) {
        if (err) {
            return console.log(err.message);
        }

        const mailOptions = {
            from: 'no-reply@reddebicicletas.com',
            to: emailDestination,
            subject: 'Account Verification',
            text: 'Hi,\n\n' + 'Please, to verify your account, click on the following link:\n\n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return console.log(err.message);
            }

            console.log('Email send to ' + emailDestination + '.');
        });
    });
};

usuarioShema.methods.resetPassword = function (cb) {
    const token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString('hex')
    });
    const email_destination = this.email;
    token.save(function (err) {
        if (err) {
            return cb(err);
        }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password',
            text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link:\n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {
                return cb(err);
            }
            console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
        });

        cb(null);
    });
};

usuarioShema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log('el email is ', condition.emails[0].value)

    this.findOne({
        $or: [
            { 'facebookId': condition.id },
            { 'email': condition.emails[0].value }
        ]
    }, 
    (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            let values = {};

            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');

            self.create(values, function (err, user) {
                if (err) {
                    console.log(err);
                }
                return callback(err, user);
            });
        }
    });
}

usuarioShema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    this.findOne({
            $or: [{
                    'googleId': condition.id
                },
                {
                    'email': condition.emails[0].value
                }
            ]
        },
        (err, result) => {
            if (result) {
                callback(err, result);
            } else {
                let values = {};

                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                self.create(values, function (err, user) {
                    if (err) {
                        console.log(err);
                    }
                    return callback(err, user);
                });
            }
        });
}


module.exports = mongoose.model("Usuario", usuarioShema);